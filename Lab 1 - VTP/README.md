# Lab 1 - VTP

Diagram

![alt text](https://gitlab.com/erik.pope/portshift-labs/-/raw/master/Lab%201%20-%20VTP/HomeLab_L2.png "Network Diagram")

My switches are old so I needed to add this config to my trunk ports for VTP to work

```
 switchport trunk encapsulation dot1q
 switchport mode trunk
```

Show Trunks

```
S-P1#show interfaces trunk

Port        Mode         Encapsulation  Status        Native vlan
Gi1/0/1     on           802.1q         trunking      1
Gi1/0/2     on           802.1q         trunking      1
Gi1/0/23    on           802.1q         trunking      1
Gi1/0/24    on           802.1q         trunking      1
```

Here is the commands I used for each switch to setup VTP

```
vtp ver 2
vtp domain PortShift
vtp password PortShift
vtp mode server
```

Add Vlans

```
S-P1(config)#vlan 100
S-P1(config-vlan)#name Data
S-P1(config-vlan)#vlan 200
S-P1(config-vlan)#name VOIP
S-P1(config-vlan)#vlan 300
S-P1(config-vlan)#name Server
```

VTP Status

```
S-P1#show vtp status
VTP Version                     : 2
Configuration Revision          : 5
Maximum VLANs supported locally : 1005
Number of existing VLANs        : 8
VTP Operating Mode              : Server
VTP Domain Name                 : PortShift
VTP Pruning Mode                : Disabled
VTP V2 Mode                     : Enabled
VTP Traps Generation            : Disabled
MD5 digest                      : 0xB0 0x40 0xE7 0xE6 0xB5 0x24 0x5F 0xE9
Configuration last modified by 0.0.0.0 at 3-1-93 21:07:01
Local updater ID is 0.0.0.0 (no valid interface found)
```


Passwords

```
Line con/VTY = portshift
Secret = portshift
```